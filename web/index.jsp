<%--
  Created by IntelliJ IDEA.
  User: Alican-PC
  Date: 7.04.2020
  Time: 20:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Demo Deneme</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link href="${pageContext.request.contextPath}/assets/css/site.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
  <nav class="navbar navbar-expand-lg navbar-light">
    <a class="navbar-brand center" href="#">
      <img src="${pageContext.request.contextPath}/assets/images/logo.jpg" />

    </a>
    <button class="navbar-toggler first-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <div class="animated-icon1"><span></span><span></span><span></span></div>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="#">Anasayfa <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Kartlar</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Krediler</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#">Bireysel İnternet Şubesi</a>
        </li>
      </ul>
    </div>
  </nav>
</div>
<div class="body-content">
<div class="container">
<div class="row">
  <div class="col col-md-4">
    <img src="${pageContext.request.contextPath}/assets/images/gbanner.jpg" />
  </div>
  <div class="col col-md-8">
    <div id="Form1Div">
      <div class="loginTitle">
        <span>AL MAYARIM BANK <br> Bireysel İnternet Şubenize Hoş Geldiniz!</span>
      </div>
      <form>
        <div class="form-group">
          <input type="email" class="form-control form-control-lg" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Müşteri Numaranız / TCKN / YKN">
          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
          <input type="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Şifre ya da Geçici Şifre">
        </div>
        <button type="submit" class="btn btn-primary  float-right">Giriş</button>
      </form>
    </div>
  </div>
</div>

</div>
</div>
<div class="clearfix"></div>
<footer class="footer">
  <div class="container">
    <p>&copy; 2020 - Demo Uygulaması</p>
  </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

<script type="text/javascript">
  $(document).ready(function () {
    $('.first-button').on('click', function () {

      $('.animated-icon1').toggleClass('open');
    });
  });
</script>
</body>
</html>
